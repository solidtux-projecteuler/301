use std::ops::BitXor;

fn main() {
    let n: u32 = (2 as u32).pow(30) + 1;
    let res: u32 = (1..n)
        .map(|x| {
            if x.bitxor(2 * x).bitxor(3 * x) == 0 {
                1
            } else {
                0
            }
        })
        .sum();
    println!("{}", res);
}
